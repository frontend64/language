// Angular Core
import {Component} from '@angular/core';

// Service
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {
    public header = {
        action: {title: 'Записаться на обучение'}
    };

    constructor(public store: StoreService) {}

    public openHome(): void {
        this.store.moveToSlide(1);
    }

    public signUp(): void {
        this.store.openPopup('feedback');
    }
}
