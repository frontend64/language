import {Component, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CookieService} from 'ngx-cookie-service';

declare const ActiveXObject: (type: string) => void;

@Component({
    selector: 'app-feedback',
    templateUrl: './feedback.component.html',
    encapsulation: ViewEncapsulation.None
})
export class FeedbackComponent {
    private form: FormGroup;
    private name: FormControl;
    private phone: FormControl;
    private email: FormControl;
    private comment: FormControl;
    public isComplete = false;
    public man = '/assets/images/screen-1/cameraman.png';

    constructor(private cook: CookieService) {
        this.createForm();
    }

    private createForm() {
        this.name = new FormControl(null, Validators.required);
        this.phone = new FormControl(null, [Validators.required, Validators.minLength(14)]);
        this.email = new FormControl(null, [Validators.required, Validators.email]);
        this.comment = new FormControl('');

        this.form = new FormGroup({
            pa_full_name: this.name,
            pa_phone: this.phone,
            pa_email: this.email,
            A4FF49A3375D9AE4: this.comment
        });
    }

    public sendDataForm() {
        const data = {
            'class': 'Record',
            action: 'addCrmRecord',
            type: 'form',
            site: '1431',
            table: '2252584',
            client: this.cook.get('pa_1431_client_id'), // id клиента, из cookie
            visit: this.cook.get('pa_1431_visit_id'), // id визита, из cookie
            host: this.cook.get('pa_1431_host'), // хост, не удалять
            fields: this.form.value,
            system: {},
        };

        if (this.form.valid) {
            this.paPost(data, this.setStatus);
            // this.anal.sendForm(data, this.setStatus);
            this.isComplete = true;
            this.form.reset();
        }
    }

    private setStatus(status) {
        console.log(status);
        (status === 'success') ? this.isComplete = true : null;
    }

    private paPost(object, callback): void {
        let xhr;
        if ((<any>window).ActiveXObject) {
            xhr = new ActiveXObject('Microsoft.XMLHTTP');
        } else {
            xhr = new XMLHttpRequest();
        }
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                callback(JSON.parse(xhr.responseText));
            }
        };
        xhr.open('POST', 'https://analytics.prostoy.ru/api/v1/records/crm', true); // true for asynchronous
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send(JSON.stringify(object));
    }
}