import {Component} from '@angular/core';

@Component({
    selector: 'app-pay-form',
    templateUrl: './pay-form.component.html',
})
export class PayFormComponent {

    public description = 'English+Movies=KinoLanguage. Короткий путь в английский язык.';
}
