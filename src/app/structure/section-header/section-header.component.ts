import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-section-header',
    templateUrl: './section-header.component.html'
})
export class SectionHeaderComponent implements OnInit {
    @Input() sectionName = 'Название';
    icon = '/assets/images/rect.png';

    constructor() {}

    ngOnInit() {
    }

}
