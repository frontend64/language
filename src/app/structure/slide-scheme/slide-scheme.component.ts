import {Component, Input, OnInit} from '@angular/core';
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-slide-scheme',
    templateUrl: './slide-scheme.component.html',
})
export class SlideSchemeComponent implements OnInit {
    @Input() scheme: any;
    @Input() number: number;

    constructor(private store: StoreService) { }

    ngOnInit() {}

    public openModal(id) {
        this.store.openPopup(id);
    }
}
