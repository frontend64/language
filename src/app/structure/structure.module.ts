// Angular Core
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// Components
import {HeaderComponent} from './header/header.component';
import {SlideSchemeComponent} from './slide-scheme/slide-scheme.component';
import {SectionHeaderComponent} from './section-header/section-header.component';
import {FeedbackComponent} from './feedback/feedback.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IMaskModule} from 'angular-imask';
import { PayFormComponent } from './pay-form/pay-form.component';

@NgModule({
    declarations: [
        HeaderComponent,
        SlideSchemeComponent,
        SectionHeaderComponent,
        FeedbackComponent,
        PayFormComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        IMaskModule,
        ReactiveFormsModule
    ],
    exports: [
        HeaderComponent,
        SlideSchemeComponent,
        SectionHeaderComponent,
        FeedbackComponent,
        PayFormComponent
    ]
})
export class StructureModule {
}
