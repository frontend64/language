import {AfterContentInit, Component, EventEmitter, Input, Output} from '@angular/core';
import {trigger, transition, useAnimation} from '@angular/animations';
import {rotateIn} from 'ng-animate';
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-educator',
    templateUrl: './educator.component.html',
    animations: [
        trigger('bounce', [transition('* => *', useAnimation(rotateIn, {
            // Set the duration to 5seconds and delay to 2seconds
            params: {timing: 0.3, delay: .2}
        }))])
    ],
})
export class EducatorComponent implements AfterContentInit {
    @Input() position: any;
    @Output() goBack = new EventEmitter();
    @Output() setSlide = new EventEmitter();

    bounce: any;
    public content: any;
    public avatars: any;
    public instructor: any;

    constructor(public store: StoreService) {
        this.content = this.store.education;
    }

    ngAfterContentInit(): void {
        this.avatars = this.store.education.instructor;
        this.instructor = this.store.education.instructor[this.position];
    }

    public showPopup() {
        this.store.openPopup('educator', this.instructor);
    }

    /** */
    public openVideo(): void {
        window.open('https://www.youtube.com/watch?v=7XxU7LcRwCA');
    }

    public checkIsHasVideo(): boolean {
        return (this.instructor.index === 0);
    }

    public setActiveItem(index): string {
        return this.instructor.index === index ? 'active-link' : null;
    }

    public setSlideItem(item) {
        this.setSlide.emit(item);
        console.log(item);
    }
}
