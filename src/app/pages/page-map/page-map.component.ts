import {ChangeDetectionStrategy, Component} from '@angular/core';
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-page-map',
    templateUrl: './page-map.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageMapComponent {
    public content: any;
    public logo = '/assets/images/screen-map/logo_map.png';

    constructor(private store: StoreService) {
        this.content = this.store.map;
    }

    /** Переадресация */
    public clickEvents(item) {
        if (item.url) {
            window.open(item.url, '_blank');
        }
    }
}
