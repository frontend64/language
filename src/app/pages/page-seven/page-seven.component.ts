import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'app-pages-seven',
    templateUrl: './page-seven.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageSevenComponent {
    content = [
        {
            name: 'Вводная лекция',
            text: 'Первое обязательное занятие базового курса',
            price: '500',
            desc: ''//'Лекция включает в себя рассках о методике, просмотр клипов, мкльтипликационных и короткометражных фильмов, а также основопологающий тренинг по фонетике'
        },
        {
            name: 'Основной курс',
            text: 'Групповые или индивидуальные занятия, 53 ак. часа',
            price: 'от 28 000',
            desc: 'Систематический курс обучения языку по фильмам с динамически меняющейся структурой уроков'
        },
        {
            name: 'Полный курс',
            text: '53 ак. часа + выездной тренинг',
            price: 'от 40000',
            desc: '* В тренинге могут участвовать только ученики, закончившие 2 предыдущих этапа обучения'
        },
    ];

}
