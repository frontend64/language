import {Component, Input, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-popup',
    templateUrl: './popup.component.html',
})
export class PopupComponent {
    @Input() modalName: any = 'modal';
    @ViewChild('method', {read: TemplateRef, static: false}) method: TemplateRef<any>;
    @ViewChild('educator', {read: TemplateRef, static: true}) educator: TemplateRef<any>;
    @ViewChild('movies', {read: TemplateRef, static: true}) movies: TemplateRef<any>;
    @ViewChild('page_a', {read: TemplateRef, static: true}) page_a: TemplateRef<any>;
    @ViewChild('page_c', {read: TemplateRef, static: true}) page_c: TemplateRef<any>;
    @ViewChild('events', {read: TemplateRef, static: true}) events: TemplateRef<any>;
    @ViewChild('feedback', {read: TemplateRef, static: true}) feedback: TemplateRef<any>;
    @ViewChild('payform', {read: TemplateRef, static: true}) payform: TemplateRef<any>;
    public modalTmp: TemplateRef<any>;
    public type: any = null;
    public data: any;
    public eventsMass: any;

    constructor(private store: StoreService) {
        this.eventsMass = this.store.eventsEveryDay;
    }

    public clearText(text: string): string {
        return text.replace('<br>', ' ');
    }

    public closePopup() {
        this.type = null;
        this.modalTmp = null;
    }

    /** */
    public logging(): void {
        switch (this.type) {
            case 'method':
                console.log(555);
                this.modalTmp = this.method;
                break;
            case 'movies':
                this.modalTmp = this.movies;
                break;
            case 'educator':
                this.modalTmp = this.educator;
                break;
            case 'page_a':
                this.modalTmp = this.page_a;
                break;
            case 'page_c':
                this.modalTmp = this.page_c;
                break;
            case 'events':
                this.modalTmp = this.events;
                break;
            case 'feedback':
                this.modalTmp = this.feedback;
                break;
            case 'payform':
                this.modalTmp = this.payform;
                break;
        }
    }
}
