import {Component} from '@angular/core';
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-warner-page',
    templateUrl: './warner-page.component.html',
})
export class WarnerPageComponent {
    constructor(public store: StoreService) {

    }

    public openPayForm(): void {
        this.store.openPopup('payform');
    }

}
