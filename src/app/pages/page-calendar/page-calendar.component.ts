// Angular Core
import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    Renderer2,
    SimpleChanges
} from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';
import {StoreService} from '../../shared/__service/store.service';

export interface CalendarDate {
    mDate: moment.Moment;
    selected?: boolean;
    today?: boolean;
}

@Component({
    selector: 'app-page-calendar',
    templateUrl: './page-calendar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageCalendarComponent implements OnInit, OnChanges {
    @Input() selectedDates: CalendarDate[] = [];
    @Output() onSelectDate = new EventEmitter<CalendarDate>();
    // public currentDate = moment('09-01-2019', 'MM-DD-YYYY').locale('ru');
    public currentDate = moment().locale('ru');
    public plusOneMonth = moment().locale('ru').add(1, 'months');
    public dayNames: any;
    public content: any;
    public weeks: CalendarDate[][] = [];
    public sortedDates: CalendarDate[] = [];
    public eventsDay = new Map();
    public constantlyEvents = new Map();
    public lastArea: any;

    public arrowIcon = '/assets/images/arrow.png';

    constructor(public store: StoreService, private ren: Renderer2) {
        /** Merge */
        this.constantlyEvents.set(1, {
            day: '1',
            start: '2019-09-02',
            stop: '2019-09-03',
            text: '«Merge: разговорное занятие по кино. 8 (903) 724-71-58 Александр»',
        });
        this.constantlyEvents.set(4, {
            day: '4',
            start: '2019-09-05',
            stop: '2019-09-06',
            text: '«Merge: разговорное занятие по кино. 8 (903) 724-71-58 Александр»',
        });
        this.constantlyEvents.set(6, {
            day: '6',
            start: '2019-08-31',
            stop: '2019-09-07',
            text: '«Merge: разговорное занятие по кино. 8 (903) 724-71-58 Александр»',
        });
        this.constantlyEvents.set(6, {
            day: '6',
            start: '2020-01-31',
            stop: '2020-02-07',
            text: 'Новое событие',
        });

        /** Регулярные события */
        this.eventsDay.set('07.09.19', {
            text: 'Тренинг "Английский по кино с KinoLanguage".',
            url: 'https://kinolanguage-school.timepad.ru/event/985587/'
        });
        this.eventsDay.set('16.09.19', {
            text: 'Регулярные занятия по группам, курс осень-зима 2019',
            url: 'https://kinolanguage-school.timepad.ru/event/985584/'
        });
        this.eventsDay.set('20.09.19', {
            text: 'Регулярные занятия по группам, курс осень-зима 2019',
            url: 'https://kinolanguage-school.timepad.ru/event/985584/'
        });
        this.eventsDay.set('21.09.19', {
            text: 'Регулярные занятия по группам, курс осень-зима 2019',
            url: 'https://kinolanguage-school.timepad.ru/event/985584/'
        });
        this.eventsDay.set('23.09.19', {
            text: 'Регулярные занятия по группам, курс осень-зима 2019',
            url: 'https://kinolanguage-school.timepad.ru/event/985584/'
        });
        this.eventsDay.set('27.09.19', {
            text: 'Регулярные занятия по группам, курс осень-зима 2019',
            url: 'https://kinolanguage-school.timepad.ru/event/985584/'
        });
        this.eventsDay.set('28.09.19', {
            text: 'Регулярные занятия по группам, курс осень-зима 2019',
            url: 'https://kinolanguage-school.timepad.ru/event/985584/'
        });
        this.eventsDay.set('30.09.19', {
            text: 'Регулярные занятия по группам, курс осень-зима 2019',
            url: 'https://kinolanguage-school.timepad.ru/event/985584/'
        });
        this.eventsDay.set('27.01.20', {
            text: 'Тренинг "Английский по кино с KinoLanguage", Полина Пономарёва',
            url: 'https://kinolanguageschool.timepad.ru/event/1235085/'
        });

        this.content = this.store.calendar;
        this.dayNames = this.store.calendar.dayNames;
    }

    ngOnInit(): void {
        this.generateCalendar();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.selectedDates && changes.selectedDates.currentValue && changes.selectedDates.currentValue.length > 1) {
            // sort on date changes for better performance when range checking
            this.sortedDates = _.sortBy(changes.selectedDates.currentValue, (m: CalendarDate) => m.mDate.valueOf());
            this.generateCalendar();
        }
    }

    /** Проверяем Регулярные события и дабвляем класс*/
    public regularEvents(day): boolean {
        const number = day.mDate.days();
        const weekDay = day.mDate.day().toString();
        const data = this.constantlyEvents.get(number);
        const full = day.mDate.format('YYYY-MM-DD').toString();

        /** Проверяем дату начала с текущей день недели с текущим и дата окончания > текущей*/
        return (data) ? ((full === data.start) && (data.day === weekDay) && (moment(full).isBefore(data.stop)) || full === data.stop) : false;
    }

    /** Проверяем текущие события и дабвляем класс*/
    public checkEventsDate(day): boolean {
        return this.checkIsDayHas(this.getDayFull(day));
    }

    public checkDetailDay(day): boolean {
        return (this.checkEventsDate(day) || this.regularEvents(day));
    }

    public showDetail(area) {
        (this.lastArea) ? this.ren.removeClass(this.lastArea, 'hover-area--detail') : null;
        this.lastArea = area.target;
        this.ren.addClass(area.target, 'hover-area--detail');
    }

    public closeDetail(area) {
        this.ren.removeClass(area, 'hover-area--detail');
    }

    private checkIsDayHas(day): any {
        return this.eventsDay.has(day);
    }

    public getRegular(day): any {
        const number = day.mDate.days();
        if (this.regularEvents(day)) {
            const events = this.constantlyEvents.get(number);
            return (events) ? events.text : null;
        }
        return null;
    }

    public getEventsName(day): any {
        const events = this.eventsDay.get(this.getDayFull(day));
        return (events && events.text) ? events.text : null;
    }

    private getDayFull(day): string {
        return day.mDate.format('DD.MM.YY').toString();
    }

    public selectDate(date: CalendarDate): void {
        const fulldate = this.getDayFull(date);
        const event = this.eventsDay.get(fulldate);

        // tslint:disable-next-line:no-unused-expression
        (event.url) ? window.open(event.url) : null;
        this.onSelectDate.emit(date);
    }

    public regularClick(date: CalendarDate): void {
        this.onSelectDate.emit(date);
    }

    public isToday(date: moment.Moment): boolean {
        return moment().isSame(moment(date), 'day');
    }

    public isSelected(date: moment.Moment): boolean {
        return _.findIndex(this.selectedDates, (selectedDate) => {
            return moment(date).isSame(selectedDate.mDate, 'day');
        }) > -1;
    }

    /** Проверяем это текущий или следующий месяц */
    public isSelectedMonth(date: moment.Moment): boolean {
        return moment(date).isSame(this.currentDate, 'month') ||
            moment(date).isSame(this.plusOneMonth, 'month');
    }

    // generate the calendar grid
    public generateCalendar(): void {
        const dates = this.fillDates(this.currentDate);
        const weeks: CalendarDate[][] = [];
        while (dates.length > 0) {
            weeks.push(dates.splice(0, 7));
        }
        this.weeks = weeks;
    }

    public fillDates(currentMoment: moment.Moment): CalendarDate[] {
        const firstOfMonth = moment(currentMoment).startOf('month').day();
        const firstDayOfGrid = moment(currentMoment).startOf('month').subtract(firstOfMonth, 'days');
        const start = firstDayOfGrid.date();
        return _.range(start, start + 42)
            .map((date: number): CalendarDate => {
                const d = moment(firstDayOfGrid).date(date);
                return {
                    today: this.isToday(d),
                    selected: this.isSelected(d),
                    mDate: d,
                };
            });
    }

    // actions from calendar

    public prevMonth(): void {
        this.currentDate = moment(this.currentDate).subtract(1, 'months');
        this.generateCalendar();
    }

    public nextMonth(): void {
        this.currentDate = moment(this.currentDate).add(1, 'months');
        this.generateCalendar();
    }

    /*
    public firstMonth(): void {
        this.currentDate = moment(this.currentDate).startOf('year');
        this.generateCalendar();
    }

    public lastMonth(): void {
        this.currentDate = moment(this.currentDate).endOf('year');
        this.generateCalendar();
    }

    public prevYear(): void {
        this.currentDate = moment(this.currentDate).subtract(1, 'year');
        this.generateCalendar();
    }

    public nextYear(): void {
        this.currentDate = moment(this.currentDate).add(1, 'year');
        this.generateCalendar();
    }
    */
}
