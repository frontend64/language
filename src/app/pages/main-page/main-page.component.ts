import {ChangeDetectionStrategy, Component} from '@angular/core';
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-main-page',
    templateUrl: './main-page.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainPageComponent {
    public man = "/assets/images/screen-1/cameraman.png";
    public first = 'Авторский курс английского языка по фильмам';
    slogan = [
        {title: 'Красивое британское произношение <br> за 7 занятий'},
        {title: 'Колоссальный скачок лексического запаса на 8-10<br>тысяч слов'},
        {title: 'Возможность вести беседы с носителями языка'},
    ];

    constructor(public store: StoreService) {
    }

    /** Сменить слайд */
    public nextSlide(): void {
        this.store.moveSliderNext();
    }

    public openCalendar():void{
        this.store.moveToSlide(9);
    }
}
