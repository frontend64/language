import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-page-select-movies',
    templateUrl: './page-select-movies.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageSelectMoviesComponent implements OnInit {
    public metaText: string;

    constructor(private store: StoreService) {
        this.metaText = this.store.selectMovies.metaText;
    }

    ngOnInit() {}

    /**------------------------------------------------------------------------------- */
    public openMovieList() {
        this.store.openPopup('movies');
    }
}
