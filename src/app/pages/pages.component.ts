import {
    AfterViewInit,
    Component,
    HostListener,
    Input,
    OnInit, Renderer2,
    ViewChild,
} from '@angular/core';
import {MnFullpageOptions, MnFullpageService} from 'ngx-fullpage';
import {PopupComponent} from './popup/popup.component';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {StoreService} from '../shared/__service/store.service';


declare var grained: any;

@Component({
    selector: 'app-pages',
    templateUrl: './pages.component.html',
})
export class PagesComponent implements OnInit, AfterViewInit {
    private up: Map<string, string>;
    private down: Map<string, string>;
    @Input() public options: MnFullpageOptions = MnFullpageOptions.create({
        // anchors: ['page1', 'page2', 'page3', 'page4'],
        controlArrows: false,
        scrollingSpeed: 700,
    });
    @ViewChild(PopupComponent, {read: PopupComponent, static: true}) popup: PopupComponent;
    public mouse: any;
    educatorGif: any;
    screenId: any[] = ['screen-1', 'warner', 'screen-2', 'screen-3', 'screen-4', 'screen-5', 'screen-6', 'screen-7', 'screen-8', 'screen-9', 'select-movies'];
    settings = {
        animate: true,
        patternWidth: 100,
        patternHeight: 100,
        grainOpacity: 0.15,
        grainDensity: 1,
        grainWidth: 1,
        grainHeight: 1,
        fitToSection: true,
        css3: true
    };

    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
        if (this.popup.type === null) {
            if (this.down.has(event.key)) {
                this.slider.moveSectionDown();
            } else if (this.up.has(event.key)) {
                this.slider.moveSectionUp();
            }
        }
    }

    constructor(private store: StoreService,
                private ren: Renderer2,
                public modal: NgxSmartModalService,
                private slider: MnFullpageService) {
        this.down = new Map();
        this.up = new Map<string, string>();
        this.down.set('PageDown', 'PageDown');
        this.down.set('ArrowDown', 'ArrowDown');
        // this.down.set('ArrowRight', 'ArrowRight');

        this.up.set('PageUp', 'PageUp');
        this.up.set('ArrowUp', 'ArrowUp');

        this.educatorGif = this.store.animateBs.asObservable();
        this.mouse = this.store.mouse;
        this.store.getPopupData().subscribe((data) => this.openPopup(data));
    }

    ngOnInit(): void {
        this.screenId.forEach((screen) => {
            grained('#'.concat(screen), this.settings);
        });
    }

    ngAfterViewInit() {
        this.addNavName();
    }

    private addNavName(): void {
        const title = [
            'Плавный', 'Методика', 'Состав',
            'Галерея', 'Преподаватели', 'Фильмы',
            'Отзывы', 'Стоимость', 'Календарь', 'Контакты'];
        const nav = document.getElementById('fp-nav')
            .getElementsByTagName('li');

        // Array.from(nav).forEach((item, index) => {
        //     this.ren.setAttribute(item, 'title', title[index]);
        // });
    }


    public openPopup(data): void {
        if (data) {
            this.popup.data = data?.data;
        }
        this.modal.resetModalData('modal');
        this.popup.type = data?.id;
        this.modal.open('modal');
    }

    public moveSliderDown(): void {
        this.store.moveSliderNext();
    }

    public sliderUp(event): void {
        console.log('slider up', event);
    }
}
