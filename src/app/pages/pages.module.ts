/** Angular Core */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/** Modules */
import {StructureModule} from '../structure/structure.module';
import {PagesRoutingModule} from './pages-routing.module';
import {MnFullpageModule} from 'ngx-fullpage';
/** Componenets */
import {MainPageComponent} from './main-page/main-page.component';
import {PagesComponent} from './pages.component';
import {PopupComponent} from './popup/popup.component';
import {NgxSmartModalModule} from 'ngx-smart-modal';
import {PageSecondComponent} from './page-second/page-second.component';
import {PageShedulerComponent} from './page-sheduler/page-sheduler.component';
import {PageGalleryComponent} from './page-gallery/page-gallery.component';
import {PageFiveComponent} from './page-five/page-five.component';
import {EducatorComponent} from './educator/educator.component';
import {PageSelectMoviesComponent} from './page-select-movies/page-select-movies.component';
import {PageCommentComponent} from './page-comment/page-comment.component';
import {PageSevenComponent} from './page-seven/page-seven.component';
import {PageCalendarComponent} from './page-calendar/page-calendar.component';
import {PageMapComponent} from './page-map/page-map.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CookieService} from 'ngx-cookie-service';
import {WarnerPageComponent} from './warner-page/warner-page.component';

@NgModule({
    declarations: [
        PagesComponent,
        MainPageComponent,
        PopupComponent,
        PageSecondComponent,
        PageShedulerComponent,
        PageGalleryComponent,
        PageFiveComponent,
        EducatorComponent,
        PageSelectMoviesComponent,
        PageCommentComponent,
        PageSevenComponent,
        PageCalendarComponent,
        PageMapComponent,
        WarnerPageComponent
    ],
    imports: [
        CommonModule,
        StructureModule,
        MnFullpageModule,
        PagesRoutingModule,
        NgxSmartModalModule,
        ReactiveFormsModule,
    ],
    providers: [
        CookieService
    ]
})
/** */
export class PagesModule {
}
