import {Component} from '@angular/core';
// Service
import {MnFullpageService} from 'ngx-fullpage';
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-page-five',
    templateUrl: './page-five.component.html',
})
export class PageFiveComponent {
    public index: number;
    public content: any;

    constructor(private slider: MnFullpageService, private store: StoreService) {
        this.content = this.store.education.instructor;
    }

    public nextPage(item) {
        this.store.setAnimationForEducator(item.animation);
        this.index = item.index;
        this.slider.moveSlideRight();
    }

    public backPage(): void {
        this.store.setDefaultAnimation();
        this.slider.moveSlideLeft();
    }

    public moveTo(item): void {
        this.index = item.index;
        this.store.setAnimationForEducator(item.animation);
    }

}
