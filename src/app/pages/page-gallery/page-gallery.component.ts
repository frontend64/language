import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'app-gallery',
    templateUrl: './page.gallery.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageGalleryComponent {

    constructor() {}
}
