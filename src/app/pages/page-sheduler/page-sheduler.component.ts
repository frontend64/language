import {Component} from '@angular/core';
import {MnFullpageService} from 'ngx-fullpage';
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-page-sheduler',
    templateUrl: './page-sheduler.component.html',
})
export class PageShedulerComponent {
    scheme: any;

    constructor(private slider: MnFullpageService, public store: StoreService) {
        this.scheme = this.store.sheduler;
    }

    /**-------------------------------------------------------------------------------------------------------------- */
    public nextPage() {
        this.slider.moveSlideRight();
    }

    /**-------------------------------------------------------------------------------------------------------------- */
    public prevPage() {
        this.slider.moveSlideLeft();
    }

    /**-------------------------------------------------------------------------------------------------------------- */
    /** */
    public openVideo(): void {
        window.open('https://www.youtube.com/watch?v=h6QfvOvkeMM');
    }
}
