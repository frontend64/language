// Angular Core
import {ChangeDetectionStrategy, Component} from '@angular/core';
// Service
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-comment',
    templateUrl: './page-comment.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageCommentComponent {
    public content: any;

    constructor(private store: StoreService) {
        this.content = this.store.comments;
    }

}
