import {Component} from '@angular/core';
import {StoreService} from '../../shared/__service/store.service';

@Component({
    selector: 'app-second-page',
    templateUrl: './page-second.component.html',
})
export class PageSecondComponent {
    public textFrame = {
        title: 'О методике',
        first: 'KinoLanguage — это авторская методика изучения английского языка, которая помогает в короткий срок заметно улучшить понимание английского на слух  и снять языковой барьер.',
        second: ' Методика школы основана на подключении к потоку живой речи и постановке произношения, грамотном аудировании и комплексной проработке лексики, грамматики, синтаксиса на основе фраз и диалогов'
    };
    images = [
        {src: '/assets/images/screen-2/1.png', up: 'Постановка произношения', down: '25 академических часов'},
        {src: '/assets/images/screen-2/2.png', up: 'Набор лексики и ключевые темы по грамматике', down: '28 академических часов'},
        {
            src: '/assets/images/screen-2/3.png',
            up: 'Переход на общение только на английском',
            down: 'Выездной тренинг «Only English Please»'
        },
    ];

    constructor(private  store: StoreService) {
    }


    public openModal() {
        this.store.openPopup('method');
    }
}
