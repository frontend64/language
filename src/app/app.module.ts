import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {NgxSmartModalModule} from 'ngx-smart-modal';
import {MnFullpageModule} from 'ngx-fullpage';


@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule,
        MnFullpageModule,
        AppRoutingModule,
        NgxSmartModalModule.forRoot(),
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
