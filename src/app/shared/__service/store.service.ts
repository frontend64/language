import {Injectable} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {MnFullpageService} from 'ngx-fullpage';
import {BehaviorSubject, Observable} from 'rxjs';
import {skip} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class StoreService {
    public imagePath = '/assets/images/';
    public mouse = '/assets/images/mouse_icon.png';
    public line = '/assets/images/screen-1/line.png';
    public arrowPrev = this.imagePath.concat('/screen-3/arrow-prev.png');
    public arrowNext = this.imagePath.concat('/screen-3/arrow-next.png');
    public media = {
        logo: '/assets/images/logo.png',
        border: '/assets/images/pattern-border.png',
    };
    public comments = {
        first: [
            {image: '/assets/images/comments/review-6.png', author: 'Сергей Краснов'},
            {image: '/assets/images/comments/review-3.png', author: 'Ксения Рудич'},
            {image: '/assets/images/comments/review-5.png', author: 'Наталья Парахина'},
        ],
        second: [
            {image: '/assets/images/comments/review-2.png', author: 'Ксения Рудич'},
            {image: '/assets/images/comments/review-4.png', author: 'Светлана Чижкова'},
            {image: '/assets/images/comments/review-1.png', author: 'Наталья Дружинина'},
        ],
    };

    /** Контакты */
    public map = {
        map: this.imagePath.concat('screen-map/map.png'),
        contacts: [
            {name: 'Адрес', text: 'Москва, Котельническая наб, 25 к. 2, подъезд 7', link: false},
            {name: 'Телефон', text: '+7 903 123 55 90', link: false},
            {name: 'Электронная почта', text: 'info@kinolanguage.ru', link: true},
            {name: 'Социальные сети', text: 'Мы на Facebook', url: 'https://facebook.com', link: true},
        ]
    };

    public education = {
        videoImg: '/assets/images/screen-5/watch_video.png',
        bubble: '/assets/images/screen-5/bubble.png',
        instructor: [
            {
                animation: 'animated--polina',
                profession: 'Автор методики, преподаватель',
                name: 'Полина Пономарева',
                desc: 'Привет, меня зовут Полина Пономарёва.<br>Я преподаватель английского языка, а еще психолог и лингвист по образованию (дипломы МГУ, МГППУ)\n' +
                    'Я начала учить английский с 8-ми лет, несколько лет жила в Англии. Поэтому у меня классическое британское произношение, которое я ставлю своим ученикам.\n' +
                    'Я преподаю английский более 15 лет. За это время я разработала свою собственную методику обучения английскому. Девять лет назад я основала школу Kinolanguage в которую приглашаю всех, кто хочет быстро и эффективно преодолеть языковой барьер, а также понимать и быть понятым.\n' +
                    'В обучении мы идём от пассива к активу. Проходим путь от простого к тому, что казалось сложным. Для этого приходится превращаться в ребенка и повторять за фильмом. Это сначала очень трудно, а потом начинается ощущение, что язык понятен, язык свой, и ты уже на пороге того, чтобы заговорить.\n' +
                    'А еще я люблю петь, особенно на английском.',
                img: '/assets/images/screen-5/polina_ava.png',
                index: 0,
            },
            {
                animation: 'animated--natalia',
                profession: 'Преподаватель грамматики',
                name: 'Наталья Максимова',
                desc: '«Люблю языки и путешествия, фольклорную и классическую музыку, фотографию и качественную кинематографию, антропологию и этологию, и вообще - параллели и точки соприкосновения. ' +
                    'Ещё пылко люблю грамматику, которая позволяет не только верно сориентироваться в реальном языковом пространстве, но и увидеть стройность и красоту языка».',
                img: '/assets/images/screen-5/natalia.jpg',
                index: 1,
            },
            {
                animation: 'animated--polina',
                profession: 'Ведущий выездного тренинга',
                name: 'Анна Солдаткина',
                desc: 'Cинхронист и переводчик, преподаватель свыше 16 лет, ведущая клуба "Games & Movies" и выездного тренинга "Only English Please" в школе KinoLanguage. Уже многие годы Анна помогает нашим ученикам сделать первые шаги в языковой социализации и начать уверенно применять на практике знания и навыки, которые до этого хранились в пассивном запасе. Анна с лёгкостью вовлечёт каждого в интересную дискуссию, и вы сами того не заметите, как уже без труда будете поддерживать общую беседу!',
                img: '/assets/images/screen-5/anna.jpg',
                index: 2,
            },
            {
                animation: 'animated--polina',
                profession: 'Автор методики Merge',
                name: 'Александр Афанасьев',
                desc: 'Или он же – Александр Мерге. Автор собственной Системы по самостоятельному освоению иностранного языка, как второго родного; организатор лингвистического разговорного клуба MERGE; преподаватель с опытом более 30-ти лет по различным предметам в вузах России и Гарварда; свободно владеющий Американским вариантом английского языка, освоивший его без проживания в США. Активный гражданин России, стремящийся научить россиян, как поднять собственный уровень жизни, через использование английского языка для получения экономических преференций США, Англии и других развитых экономик.  Выпускник МИФИ, ВАВТ, МГЛУ и РЭУ им. Плеханова. В школе Kinolanguage организую просмотры фильмов и выездные тренинги по авторским методикам для продвинутых уровней, начиная с В1. \n' +
                    'И кое-что ещё, и кое-что иное, О чём не говорят, чему не учат в школе.\n',
                img: '/assets/images/screen-5/alex.jpeg',
                index: 3,
            },
        ]
    };
    public sheduler = [
        {
            id: 'page_a',
            extra: true,
            img: this.imagePath.concat('screen-3/sostav-1.png'),
            title: 'Вводное занятие',
            text: 'Лекция включает в себя рассказ о методике, просмотр клипов, мультипликационных и короткометражных фильмов, а также основополагающий тренинг по фонетике и обязательное домашнее задание.'
        },
        {
            extra: false,
            img: this.imagePath.concat('screen-3/img-2.png'),
            title: 'Индивидуальное занятие',
            text: 'Здесь происходит ломка стереотипов и выявляются основные ошибки в произношении конкретного ученика школы. По итогам занятий ученик получает набор упражнений помогающий избавиться от своих ошибок.'
        },
        {
            id: 'page_c',
            extra: true,
            img: this.imagePath.concat('screen-3/sostav-3.png'),
            title: 'Групповые занятия',
            text: 'Проводится 5 групповых занятий и зачёт. На данном этапе каждое занятие представляет собой непрерывное трехчасовое погружение в живой языковой поток путем просмотра и разбора неадаптированных фильмов на английском языке. Возможно индивидуальное обучение.',
        },
        {
            extra: false,
            img: this.imagePath.concat('screen-3/sostav-4.png'),
            title: 'Разговорный тренинг',
            text: 'Выездной тренинг «Only English Please» - это игры, обсуждения, творческие задания и свободное общение. Для того, чтобы заговорить — надо говорить. Для этого мы удаляемся на двое суток, в Подмосковье, на закрытый тренинг для учеников прошедших программу школы KinoLanguage.'
        },
        {
            extra: false,
            img: null,
            title: null,
            text: null
        },
        {
            extra: false,
            img: null,
            title: null,
            text: null
        }
    ];

    public eventsEveryDay = [
        {id: null, date: '', weekday: '', text: '', url: 'Каждый понедельник', skill: '(категория – все, advanced)'},
        {id: null, date: '', weekday: '', text: '', url: 'Каждый четверг', skill: '(категория – все, advanced)'},
        {id: null, date: '', weekday: '', text: '', url: 'Все среды с 5 июня', skill: '(категория – все, advanced)'},
    ];

    public eventsDay = [
        {id: null, date: '24', weekday: '', text: '', url: 'Каждый понедельник', skill: '(категория – все, advanced)'},
        {id: null, date: '25', weekday: '', text: '', url: 'Каждый четверг', skill: '(категория – все, advanced)'},
        {id: null, date: '26', weekday: '', text: '', url: 'Все среды с 5 июня', skill: '(категория – все, advanced)'},
        {id: null, date: '26', weekday: '', text: '', url: 'Все среды с 5 июня', skill: '(категория – все, advanced)'},
    ];

    public calendar = {
        dayNames: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        events: {eventsName: 'Вводное занятие', eventsDetail: '15:00, Маросейка, 6/8 стр. 1.'},
        close: this.imagePath.concat('screen-map/close.png')
    };
    public selectMovies = {
        metaText: 'Очень важен выбор фильма. Поскольку речь идет о медленном просмотре и разборе всех уровней языка по тексту фильма (лексика, фонетика, грамматика), то выбирать' +
            'надо произведение исскуства.<br> Мы бережно относимся к авторским правам, поэтому даем ссылки, по которой каждый из учеников скачивает на свой компьютер свою версию ' +
            'фильма и с ней работает.'
    };
    private popupData!: any;
    private popupDataBS: any = new BehaviorSubject(this.popupData);

    /** Анимация*/
    public animate: any = 'animated--five'; // Гифка по умолчанию
    public animateBs = new BehaviorSubject(this.animate);

    constructor(public modal: NgxSmartModalService, private slide: MnFullpageService) {
    }

    /**--------------------------------------------------------------------------------- */
    /** */
    public openPopup(id, data?: any): void {
        console.log(id);
        this.popupDataBS.next({id: id, data: data});
    }

    /**--------------------------------------------------------------------------------- */
    public getPopupData(): Observable<any> {
        return this.popupDataBS.asObservable().pipe(skip(1));
    }

    /**--------------------------------------------------------------------------------- */
    public moveSliderNext() {
        this.slide.moveSectionDown();
    }

    public moveToSlide(slide) {
        this.slide.moveTo(slide, 0);
    }

    /**--------------------------------------------------------------------------------- */
    public setAnimationForEducator(animate) {
        this.animateBs.next(animate);
    }

    public setDefaultAnimation() {
        this.animateBs.next(this.animate);
    }
}
