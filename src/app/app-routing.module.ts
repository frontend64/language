import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    {path: '', loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)},
    // {path: 'store', loadChildren: () => import('./store/store.module').then(m => m.StoreModule)},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
